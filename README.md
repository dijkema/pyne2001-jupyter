# pyne2001-jupyter

This is a (hopefully) simple viewer for pyned2001, converting DM (dispersion measure) to distance.

Click on the following badge to launch a binder instance to compute from a web browser:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.astron.nl%2Fdijkema%2Fpyne2001-jupyter/HEAD?labpath=pyne2001-jupyter.ipynb)

Note: after searching a bit further, I found [PyGEDM](https://github.com/FRBs/pygedm) which also contains a python interface for NED2001, and also comes with a [shiny web interface](https://apps.datacentral.org.au/pygedm/).
